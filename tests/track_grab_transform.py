# --------------------------------------------------------------
# Warp API Demo
# (C) Michael DeHaan <michael@michaeldehaan.net>, 2020
# --------------------------------------------------------------
#
# this demos hows how instruments can pull notes selectively
# from a silent guide track, which can be useful to rapidly
# construct large movements without having to sequence notes
# for every single instrument.
#
# Basically one instrument can "listen" to what is playing on another track, copy the note,
# and then use it to build chords, transpose up or down, or apply an arpeggiator.

from warpseq.api import demo
from warpseq.api.public import Api as WarpApi
from warpseq.model.slot import Slot

# ======================================================================================================================
# setup API and song

api = WarpApi()
api.song.edit(tempo=120)

# ======================================================================================================================
# setup instruments

DEVICE = demo.suggest_device(api, 'IAC Driver IAC Bus 1')

api.instruments.add('guide', device=DEVICE, channel=4, min_octave=0, base_octave=4, max_octave=10, muted=True)
api.instruments.add('lead_inst', device=DEVICE, channel=1, min_octave=0, base_octave=4, max_octave=10, muted=False)
api.instruments.add('bass_inst', device=DEVICE, channel=2, min_octave=0, base_octave=4, max_octave=10,muted=False)

# ======================================================================================================================
# setup tracks

api.tracks.add(name='guide', instrument='guide')
api.tracks.add(name='lead', instrument='lead_inst')
api.tracks.add(name='bass', instrument='bass_inst')

# ======================================================================================================================
# setup scales

api.scales.add(name='C-major', note='C', octave=0, scale_type='major')

# ======================================================================================================================
# setup patterns

api.patterns.add(name='a', slots=[
    Slot(degree=1),
])

api.patterns.add(name='parrot', scale='C-major', slots=[
    Slot(track_grab='guide')
])

# ======================================================================================================================
# setup transforms

api.transforms.add(name='bassline', divide=4, slots=[
    Slot(),
    Slot(degree_shift=4),
    Slot(degree_shift=5),
])

api.transforms.add(name='octave_down', divide=1, auto_reset=True, slots=[
    Slot(octave_shift=-1)
])

api.transforms.add(name='grab', divide=1, slots=[
    Slot(octave_shift=-1)
])

# ======================================================================================================================
# setup scenes


api.scenes.add(name='scene_1', rate=1, auto_advance=True)

# ======================================================================================================================
# setup clips

api.clips.add(name='guide', scene='scene_1', track='guide', patterns=['a'],
              repeat=16, auto_scene_advance=True)


api.clips.add(name='lead', scene='scene_1', track='bass', patterns=['parrot'], rate=1,
              transforms=[['octave_down','bassline']], repeat=16)

# ======================================================================================================================
# play starting on the first scene - Ctrl+C to exit.

api.player.loop('scene_1')
