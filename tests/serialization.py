# a low level internals test, for public API tests, see examples/api/

from warpseq.model.song import Song
from warpseq.model.device import Device
from warpseq.model.instrument import Instrument
from warpseq.model.note import Note
from warpseq.model.scale import Scale
from warpseq.model.scene import Scene
from warpseq.model.pattern import Pattern
from warpseq.model.track import Track
from warpseq.model.transform import Transform
from warpseq.model.clip import Clip

def check_serialization(cls, obj):
    data1 = obj.to_dict()
    obj2 = cls.from_dict(song, data1)
    data2 = obj2.to_dict()
    assert (data1 == data2)

song = Song(name='foo')

device = Device(name='foo')
song.add_devices([device])

instrument = Instrument(name='foo', device=device, channel=1)
song.add_instruments([instrument])

root = Note(name='C', octave=1)

scale = Scale(name='foo', root=root, scale_type='major')
song.add_scales([scale])

scene = Scene(name='foo')
song.add_scenes([scene])

track = Track(name='foo', instrument=instrument)
song.add_tracks([track])

p1 = Pattern(name='foo', slots=[4,5,6,7])
p2 = Pattern(name='bar', slots=[1,2,3,4])
song.add_patterns([p1,p2])

t1 = Transform(name='foo', slots=[1,'x'])
t2 = Transform(name='bar', slots=['x',1])
song.add_transforms([t1,t2])

clip = Clip(name='foo', patterns=[p1,p2], transforms=[t1,t2], scales=[scale])
song.add_clip(scene=scene, track=track, clip=clip)

check_serialization(Device, device)
check_serialization(Instrument, instrument)
check_serialization(Clip, clip)
