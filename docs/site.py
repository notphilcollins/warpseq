#!/usr/bin/python

import os

from rockchisel import Builder

path = os.path.dirname(os.path.realpath(__file__))

site = Builder(

    index = 'index',
	input_path  = path,
	output_path = os.path.join(path, "output"),
	theme = "rockchisel.themes.rockdoc",

	variables = dict(version = 0.1),
	page_title_template = "Warp MIDI Sequencer: {{ title }}",

	sections = {
			"Start Here": {
				"Home" : "index",
				"Installation": "installation",
				"FAQ": "faq"
			},
			"API Part 1": {
				"Intro" : "api",
				"Song" : "api_song",
				"Devices" : "api_devices",
				"Instruments" : "api_instruments",
				"Tracks" : "api_tracks",
				"Scales" : "api_scales",
				"Patterns" : "api_patterns",
				"Slots" : "api_slots",
				"Scenes": "api_scenes",
				"Clips" : "api_clips",
				"Event Loop" : "api_event_loop",
			},
			"API Part 2" : {
				"Transforms" : "api_transforms",
				"Evaluators" : "api_evaluators",
				"Data Pools" : "api_data_pools",
			},
			"Community" : {
				"Web UI Preview": "ui",
				"Warp Club": "club",
				"Contributing": "contributing",
			}
	},

	theme_options=dict(
		sidebar_background="#FF0000"
	)

)

site.build()
