<h1>Slots</h1>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
<h2>About</h2>

<p>
A slot is a step in a step sequencer that results in a note, chord, REST, or other events or actions.
</p>

<p>
Normally, when used in a {{ doc('api_patterns','Pattern') }}, the values in a slot combine with the current {{ doc('api_scales','Scale') }}
to decide what note (or notes) should play. When used with {{ doc('api_transforms','Transforms') }}, these samelots help
form the basic building blocks of powerful MIDI effects, including octave shifts, arpeggiators, and controls over pattern direction.
</p>

<p>
Values for slot parameters can be simple integers, strings, and booleans, but they can also be randomized and pulled from
a variety of sources using {{ doc('api_evaluators','Evaluators') }}.
</p>

{{ begin_info_block() }}
The <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/">API Examples</A> in the source checkout
contain a wide variety of demos showing all of these patterns in context.  Try following along with these examples
in a new browser tab.
{{ end_info_block() }}

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ table_of_contents() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Creating A Slot") }}

<p>
We've already shown the basics of pattern construction in the {{ doc('api_patterns') }} chapter. The example included
there played a 7-note scale:
</p>

{{ begin_code(language='python') }}
api.patterns.add(name='basic', slots=[
    Slot(degree=1),
    Slot(degree=2),
    Slot(degree=3),
    Slot(degree=4),
    Slot(degree=5),
    Slot(degree=6),
    Slot(degree=7),
])
{{ end_code() }}

{{ begin_info_block() }}
A slight fib: if a scale is not assigned, this will actually play the first seven notes of the 12-note Chromatic scale in
C.
{{ end_info_block() }}

Here's another simple example involving chords:

{{ begin_code(language='python') }}
api.patterns.add(name='chords', slots=[
    Slot(degree=1, chord_type='major'),
    Slot(degree=2, chord_type='major'),
    Slot(degree=3, chord_type='major'),
    Slot(degree=4, chord_type='major'),
], rate=0.25)
{{ end_code() }}

Rests are constructed as follows:

{{ begin_code(language='python') }}
api.patterns.add(name='chords', slots=[
    Slot(degree=1, chord_type='major'),
    Slot(rest=True),
    Slot(degree=3, chord_type='major'),
    Slot(rest=True),
], rate=0.25)
{{ end_code() }}

Ties are also available:

{{ begin_code(language='python') }}
api.patterns.add(name='chords', slots=[
    Slot(degree=1, chord_type='major'),
    Slot(tie=True),
    Slot(degree=3, chord_type='major'),
    Slot(tie=True),
], rate=0.25)
{{ end_code() }}

<p>
A Slot can have any of the following properties:
</p>

<table border=1 cellpadding="10px">

<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='3'><b>Pitch-Related</b></td></tr>
<tr>
  <td>note</td>
  <td>string</td>
  <td>assigns an absolute note name like "C", "Bb", or "F#". Sharps and flats are interchangeable. Do not specify the octave here. Mutually exclusive with <i>degree</i></td>
</tr>

<tr>
   <td>degree</td>
   <td>int</td>
   <td>assigns a scale degree in the current scale. In C Major, "1" would be C, and "4" would be F. If the number is higher than the number of notes in the scale, it will
   access higher octaves.  Negative indexes are not supported.</td>
</tr>
<tr>
  <td>octave</td>
  <td>int</td>
  <td>assigns the octave to the note.  If not assigned, the base octave 0 will be used, but this may be offset by the <i>base_octave</i> of the {{ doc('api_instruments','Instrument') }} if the note
  was constructed by <i>degree</i>.</td>
</tr>
<tr>
  <td>degree_shift</td>
  <td>int</td>
  <td>transposes the note up or down on the current scale by this many scale degrees</td>
</tr>
<tr>
  <td>octave_shift</td>
  <td>int</td>
  <td>transposes the note up or down on the current scale by this many octaves</td>
</tr>
<tr>
  <td>sharp</td>
  <td>bool</td>
  <td>if True, moves the selected note up a semitone. The result may be outside the current scale.</td>
</tr>
<tr>
  <td>flat</td>
  <td>bool</td>
  <td>if True, moves the selected note down a semitone. The result may be outside the current scale.</td>
</tr>
<tr>
  <td>chord_type</td>
  <td>string</td>
  <td>Turns the note into a chord, or changes the chord type if already a chord.  Included chord types are listed below.</td>
</tr>
<tr>
  <td>inversion</td>
  <td>int</td>
  <td>inverts a chord 0, 1, or 2 times. Has no effect on notes that are not already chorded.</td>
</tr>

<tr>
  <td>track_grab</td>
  <td>string</td>
  <td>replaces the note that should have played at this step with the note that last played on the named track.  Possibly restricted with <i>track_copy</i> above.</td>
</tr>
<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='3'><b>Time-Related</b></td></tr>

<tr>
  <td>repeats</td>
  <td>int</td>
  <td>if set, the given note will be repeated an extra <i>N</i> times within the current slot length.  For instance if the note was C4, and <i>N</i> was 4, a 1/16th note would turn into four back-to-back 1/64th notes.</td>
</tr>
<tr>
  <td>length</td>
  <td>float</td>
  <td>multiplies the note length by this value. 0.5 gives notes half the usual length. If <i>N</i> is greater than 1, the notes may overlap if the following notes are not a rest, which may be desirable with monosynth portamento</td>
</tr>
<tr>
  <td>delay</td>
  <td>float</td>
  <td>if positive, delays the note by this multiple of the note length.  If the note was a 1/16th note, and the delay was 0.5, it would start an
  extra 1/32nd note beyond where it would have started. If negative, the timing is instead rushed by the same amount.  The length of the note is unaffected.
  <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/11_swing.py">Used in this example to implement Swing</a></td>.
</tr>
<tr>
  <td>tie</td>
  <td>bool</td>
  <td>if True, the given note will not play, and the previous note or chord's length will be extended to fill this space</td>
</tr>

<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='3'><b>Volume-Related</b></td></tr>

<tr>
  <td>rest</td>
  <td>bool</td>
  <td>replaces this note with silence. This is best used with one of the random {{ doc('api_evaluators','Evaluators') }}. </td>
</tr>

<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='3'><b>Modulation</b></td></tr>

<tr>
  <td>velocity</td>
  <td>int</td>
  <td>sets the MIDI velocity value for this note. 0 to 127.</td>
</tr>
<tr>
  <td>ccs</td>
  <td>dict</td>
  <td>assigns one or more MIDI continuous controller values for the current track</td>
</tr>

<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='3'><b>Meta</b></td></tr>

<tr>
  <td>track_copy</td>
  <td>list of track names</td>
  <td>a bit complex - this restricts the usage of {{ doc('api_evaluators','TrackGrab') }} to only the specific named tracks, only for this particular step.  This is
  best demonstrated in <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/12_note_hocketing.py">this note hocketing example</A>. If track_copy
  is not set, any track is allowed to grab notes from this track.</td>
</tr>
<tr>
  <td>variables</td>
  <td>dict</td>
  <td>assigns one or more variables as indicated by the dictionary. These are accessed with {{ doc('api_evaluators','LoadVariable') }} </td>
</tr>


<!--------------------------------------------------------------------------------------------------------------------->
<tr><td colspan='2'><b>Pattern Control</b></td></tr>

<tr>
  <td>skip</td>
  <td>int</td>
  <td>if set, ignores this number of slots and grabs the note value from the next Nth note. This is best used with one of the random {{ doc('api_evaluators','Evaluators') }}. </td>
</tr>
<tr>
  <td>shuffle</td>
  <td>bool</td>
  <td>if True, the pattern will be reshuffled when it gets to this step. This is best used with one of the random {{ doc('api_evaluators','Evaluators') }}</td>
</tr>
<tr>
  <td>reverse</td>
  <td>bool</td>
  <td>if True, the pattern will be reversed when it gets to this this step. This is best used with one of the random {{ doc('api_evaluators','Evaluators') }}</td>
</tr>
<tr>
  <td>reset</td>
  <td>bool</td>
  <td>if True, the pattern playhead will restart at the beginning when it gets to this step. This is best used with one of the random {{ doc('api_evaluators','Evaluators') }}</td>
</tr>




</table>

{{ begin_info_block() }}
Using <i>note</i> instead of <i>degree</i> will make any notes and chords produced immune to scale transpositions or octave
shifts. This allows them to be used to sequence percussion tracks where, for example, C3 might trigger a kick drum, without
risking a scale assignment that would select different MIDI drums.  Always use <i>note</i> and <i>octave</i> and not <i>degree</i> for
percussion tracks.
{{ end_info_block() }}

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ section("Chord Types") }}

Valid values for <i>chord_types</i> are:

<ul>
   <li>minor</li>
   <li>major</li>
   <li>dim</li>
   <li>aug</li>
   <li>sus4</li>
   <li>sus2</li>
   <li>fourth</li>
   <li>power</li>
   <li>fifth</li>
   <li>M6</li>
   <li>m6</li>
   <li>dom7</li>
   <li>M7</li>
   <li>m7</li>
   <li>aug7</li>
   <li>dim7</li>
   <li>mM7</li>
</ul>

<p>
This list can be expanded grow over time.  {{ doc('contributing','Contributions') }} and ideas for new directions or random
distribution options would be welcome.
</p>

{{ begin_info_block() }}
An advanced tip: a slot cannot yet represent combinations of notes like C3 + C4 + C5. To achieve this however, it is perfectly
fine to send notes at the same instrument from multiple {{ doc('api_tracks','Tracks') }}, and {{ doc('api_transforms','Transforms') }}
could be used with {{ doc('api_evaluators','Track Grabs') }} and <i>octave_shift</i> to achieve this purpose.
{{ end_info_block() }}

<!--------------------------------------------------------------------------------------------------------------------->

<hr/>
{{ section("Suggestions & Tips") }}

<ul>
<li>don't get overwhelmed by all of the options - start small and look at the first API examples to see basic Slot usage
before learning more about what you can do - every single slot option is explained in the API examples</li>
<li>Some Slot options are more interesting and applicable when used in {{ doc('api_transforms','Transforms') }} than when used in Patterns</li>
</ul>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Next Up") }}

Now it is time for {{ doc('api_scenes','Scenes') }}. We'll eventually re-use this Slot knowledge when we get
to {{ doc('api_transforms', 'Transforms') }}.

<p>
You may also wish to choose your own adventure and
learn how to level-up Slots with {{ doc('api_evaluators','Evaluators') }}.
</p>