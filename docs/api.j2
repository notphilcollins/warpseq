<h1>API</h1>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Introduction") }}

<p>
The Python API to Warp provides a backend to everything MIDI note and composition related.  It can enable
a wide variety of algorithmic composition and generative music, embedded projects, and sonic installations, but is also
the basis of the ongoing project to build a Warp GUI.
</P>

<P>
What's here?  Lots. The API provides a grid system of scenes and tracks, device and instrument management, included and user supplied scale types,
a pattern system, a custom MIDI effects system ("transforms"), and a framework for introducing
highly controllable randomness and direction management.  Warp handles all note timing considerations, MIDI math, and basic music theory math for you.
</P>

<p>
Sorry, that's a mouthful. What we mean to say is this: <b><u>This could be the most powerful MIDI sequencing library on Earth</b></u>.
</p>

<p>
As there is a lot here, we don't want you to get overwhelmed - we suggest also reading the extensive <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/">API Examples</A>,
loading the examples - playing them - and tweaking them, will help you learn the API a lot faster than using the reference guide alone.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Running Demos") }}

<p>
To run the API demos, first {{ doc('installation', 'install warp') }} and then from your console:
</p>

{{ begin_code() }}cd warp_checkout_location
cd examples/api/
python3 any_example.py
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("MIDI Interface Considerations") }}

<p>
<A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/">The API demos</A> are written to use a specific MIDI interface. If you do not have an interface named the same
as the examples, you can set an environment variable to make the demos easy to run, rather than changing the script. On a MAC, this is done as follows:
</p>

{{ begin_code() }}WARP_MIDI_DEVICE="My Device Name" PYTHONPATH=. python3 any_example.py
{{ end_code() }}

Or alternatively:

{{ begin_code() }}export WARP_MIDI_DEVICE="My Device Name"
python3 any_example.py
{{ end_code() }}

<p>
Most of the included examples use one or two MIDI tracks, some use more. Search for "tracks" in the code and see what is set up, and try to configure some instruments (in your DAW or hardware) that sound like a good
match. If you need to tweak the track numbers in the instrument definitions in those files, that's also fine.  The drum example should use a kick and a snare drum, but the other demos will sound fine with
any polyphonic synth with a short enough decay and release time. A piano instrument would also be fine.
</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Object Model") }}

<p>
Here's a diagram of the object relationships in Warp.  Don't get too overwhelmed as when you look at the examples it will
start to fit together better:
</p>

<CENTER>
<IMG SRC="./object_model.svg">
</CENTER>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("API Design Philosophy") }}

<p>
You will notice that the API examples do not use a lot of variables, object assignments, or loops.  This is because
our ultimate goal is to build a web interface that must consume all of our object model (likely in JSON) and - ALSO we also
want to make Warp accessible to programming musicians without expert programming skill.</p>

<p>In many cases our examples will pass around the name of an object, rather than an object itself.  There also are no loops.
Rather than have a looping system, Warp <i>is</i> the looping
system.  Warp contains self contained randomness features as well, allowing access to compositional variety without
extensive programming. Thus, simple things can be constructed <i>more simply</i>, all by also paving the way to support
for a UI and everything it needs.
</p>

<p>In some cases, some objects - like <i>Evaluators</i> are also a bit of lazy-evaluating black magic. If they were evaluated instantly, as in pure
python, they would not be as useful as if they were implemented in a way that can take advantage of the event loop.</p>

<p>While not a "Domain Specific Language", it may help for you to think of Warp as one. If you need to manipulate Warp while inside the event
loop, a callback system will be available for this purpose.</p>

<!--------------------------------------------------------------------------------------------------------------------->
<hr/>
{{ section("Next Steps") }}

{{ begin_info_block() }}
Tip: open the <A HREF="https://bitbucket.org/laserllama/warpseq/src/master/examples/api/">API examples</A> in a new tab while reading the API guide.
{{ end_info_block() }}

{{ begin_info_block() }}
If you get stuck or have a question, we're always wanting to refine our
documentation, so please <A HREF="mailto:michael@michaeldehaan.net">Email Michael</A>.  We'd be glad to hear from you!
{{ end_info_block() }}

Ready to learn the API? Start with {{ doc("api_song","Song") }}.
