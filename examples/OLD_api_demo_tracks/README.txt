These demo tracks were created before the introduction of "Slot()" and would need to be ported over
to work with the recent API upgrades.

See examples/api for current API code examples.
