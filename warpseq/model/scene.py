# ------------------------------------------------------------------
# Warp Sequencer
# (C) 2020 Michael DeHaan <michael@michaeldehaan.net> & contributors
# Apache2 Licensed
# ------------------------------------------------------------------

# a scene is a set of clips that usually play together at the same time.

from .base import NewReferenceObject

class Scene(NewReferenceObject):

    __slots__ = ('name', 'scale', 'auto_advance', 'rate', '_clip_ids', 'obj_id' )

    SAVE_AS_REFERENCES = [ 'scale', ]

    def __init__(self, name=None, scale=None, auto_advance=False, rate=1, clip_ids=None, obj_id=None):

        self.name = name
        self.scale = scale
        self.auto_advance = auto_advance
        self.rate = rate
        self._clip_ids = clip_ids
        self.obj_id = obj_id

        if self._clip_ids is None:
            self._clip_ids = []

        super(Scene, self).__init__()

    def clips(self, song):
        results = [ song.find_clip(x) for x in self._clip_ids ]
        results = [ r for r in results if r is not None ]
        return results

    def add_clip(self, clip):
        if clip.obj_id not in self._clip_ids:
            self._clip_ids.append(clip.obj_id)

    def has_clip(self, clip):
        return clip.obj_id in self._clip_ids

    def remove_clip(self, clip):
        self.clip_ids = [ c for c in self._clip_ids if c != clip.obj_id ]

